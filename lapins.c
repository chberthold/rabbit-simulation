/** ------------------------------------------------------------------- *
 * @todo       Rabbit colony simulation algorithm                      *
 * @authors    Ao XIE; Cholé BERTHOLD                                  *
 * @version    3.0.1                                                   *
 *                                                                     *
 * Updates:                                                            *
 *     1. Updating the simple rabbit swarm algorithm from a fictional  *
 *        to a Fibonacci algorithm to simplify the complexity of the   *
 *        algorithm.                                                   *
 *     2. Update to calcul by year.                                    *
 * ------------------------------------------------------------------- */

#include "lapins.h"

/** ------------------------------------------------------------------- *
 * @fn       RabbitSwarmAlgo                                           *
 *                                                                     *
 * @brief    Returns number of rabbits after n years                   *
 *                                                                     *
 * @param    n {int}    Duration of this algo.                         *
 *                                                                     *
 * @return   Number of couples of rabbits after n years with Fibonacci *
 *           serie                                                     *
 * ------------------------------------------------------------------- */
int RabbitSwarmAlgo(int n)
{
    int i;
    int s1,
        s2;

    s2 = 1;

    if (n == 1 || n == 2)
    {
        printf("year: %d\tNumber of couples of rabbits: 1\n", n);
    }

    s1 = 1;

    for (i = 3; i <= n; i++)
    {
        s2 = s1 + s2; // Tumbling and adding method
        s1 = s2 - s1;
        printf("year: %d\tNumber of couples of rabbits: %d\n", i, s2);
    }

    return s2;
}

/** ------------------------------------------------------------------- *
 * @fn         judgeGender                                             *
 *                                                                     *
 * @brief      Generate gender with equal probability                  *
 *                                                                     *
 * @return     Sex [1-> male; 0 -> female]                             *
 * ------------------------------------------------------------------- */
int judgeGender(void)
{
    int    sex;
    double random;

    random = genrand_real2();

    if (random < 0.5)
        sex = 1;
    else
        sex = 0;

    return sex;
}

/** ------------------------------------------------------------------- *
 * @fn         calculChanceSurvival                                    *
 *                                                                     *
 * @brief      Survival rate based on age and time to sexual maturity  *
 *                                                                     *
 * @param      ageRabbit {int} The age of the rabbit                   *
 *                                                                     *
 * @return     Survival rate of rabbits [1-> survive; 0 -> die]        *
 * ------------------------------------------------------------------- */
int calculChanceSurvival(int ageRabbit)
{
    double chanceSurvival;
    double Outdo = 0;
    int    surviveState = 0;

    chanceSurvival = genrand_real2();

    /* If the rabbit is not mature */
    if (ageRabbit == 0)
    {
        /* If it survives */
        if (chanceSurvival <= 0.35)
            surviveState = 1;
    }

    else
    {
        /* If the rabbit is adult and under ten years old */
        if (ageRabbit < 10)
        {

            /* If it survives */
            if (chanceSurvival <= 0.6)
                surviveState = 1;
        }

        else
        {
            Outdo = ageRabbit - 9;

            /* If it survives */
            if (chanceSurvival <= (0.6 - (Outdo * 0.1)))
                surviveState = 1;
        }
    }

    return surviveState;
}

/** ------------------------------------------------------------------- *
 * @fn        BoxMuller                                                *
 *                                                                     *
 * @brief     Used to generate a standard Gaussian distribution of     *
 *            random numbers.                                          *
 *                                                                     *
 * @return    A random value in a standard Gaussian distribution       *
 * ------------------------------------------------------------------- */
double BoxMuller(void)
{
    static double u;
    static double v;
    static int    phase = 0;
    double        z;

    if (phase == 0)
    {
        u = genrand_real2();
        v = genrand_real2();
        z = sqrt(-2.0 * log(u)) * sin(2.0 * M_PI * v);
    }

    else
    {
        z = sqrt(-2.0 * log(u)) * cos(2.0 * M_PI * v);
    }

    phase = 1 - phase;

    return z;
}

/** ------------------------------------------------------------------- *
 * @fn        getTimeChildYear                                         *
 *                                                                     *
 * @brief     Randomly generate the number of times a female rabbit    *
 *            reproduces in a year                                     *
 *                                                                     *
 * @return    Number of reproduction in normal distribution            *
 * ------------------------------------------------------------------- */
int getTimesChildYear(void)
{
    int    times;
    double num;

    num = BoxMuller() + 6;

    if (num < 4.5)
        times = 4;
    else if (4.5 <= num && num < 5.5)
        times = 5;
    else if (5.5 <= num && num < 6.5)
        times = 6;
    else if (6.5 <= num && num < 7.5)
        times = 7;
    else
        times = 8;

    return times;
}

/** ------------------------------------------------------------------- *
 * @fn        getNbBaby                                                *
 *                                                                     *
 * @brief     Generate an integer between 3 and 6 with equal           *
 *            probability                                              *
 *                                                                     *
 * @return    An integer variable between 3 and 6                      *
 * ------------------------------------------------------------------- */
int getNbBaby(void)
{
    return 3 + (genrand_real2() * 3);
}

/** ------------------------------------------------------------------- *
 * @fn         getDeaths                                               *
 *                                                                     *
 * @brief      Updates the number of rabbits after one year according  *
 *             to their age and survival rate                          *
 *                                                                     *
 * @param      start {Rabbit *} The arrays of rabbits                  *
 *                                                                     *
 * @return     Survival rate of rabbits [1-> survive; 0 -> die]        *
 * ------------------------------------------------------------------- */
void getDeaths(Rabbit *start)
{
    long long int rabbit;

    /* All 15-year-old rabbit dies */
    start->females[15] = 0;
    start->males[15] = 0;

    /* For each age, starting from the oldests */
    for (int age = 14; age > -1; age--)
    {

        /* For each female rabbit of that age, determine whether it survives or not */
        for (rabbit = 0; rabbit < start->females[age]; rabbit++)
        {
            start->females[age + 1] += calculChanceSurvival(age);
        }

        start->females[age] = 0;

        /* Do the same for the males */
        for (rabbit = 0; rabbit < start->males[age]; rabbit++)
        {
            start->males[age + 1] += calculChanceSurvival(age);
        }

        start->males[age] = 0;
    }
}

/** ------------------------------------------------------------------- *
 * @fn         getAdultFemales                                         *
 *                                                                     *
 * @brief      Get the number of females adults rabbits.               *
 *                                                                     *
 * @param      rabbit {Rabbit} Struct for the rabbits.                 *
 * @return     Number of females adults rabbits.                       *
 * ------------------------------------------------------------------- */
long long int getAdultFemales(Rabbit *rabbits)
{
    long long int females = 0;

    for (int age = 1; age < 16; age++)
    {
        females += rabbits->females[age];
    }

    return females;
}

/** ------------------------------------------------------------------- *
 * @fn         getAdultMale                                            *
 *                                                                     *
 * @brief      Checks whether there is at least one male rabbit.       *
 *                                                                     *
 * @param      rabbit {Rabbit} Struct for the rabbits.                 *
 * @return     State of number the male rabbit. [1->Yes; 0->No]        *
 * ------------------------------------------------------------------- */
int getAdultMale(Rabbit *rabbits)
{
    int age = 1;

    while (age < 16 && rabbits->males[age] == 0)
    {
        age++;
    }

    return (age == 16) ? 0 : 1;
}


/** ------------------------------------------------------------------- *
 * @fn         OneYearLater                                            *
 *                                                                     *
 * @brief      Simulates one year for the rabbit population            *
 *                                                                     *
 * @param      rabbit {Rabbit} Struct for the rabbits.                 *
 * ------------------------------------------------------------------- */
void oneYearLater(Rabbit *rabbit){

    long int      babyThisYear;
    long int      babies[2];
    int           stateMale;
    int           timeThisYear;
    int           sex;
    long long int numAdultFemales;

    // First, give birth to the babies
    // For rabbits, the number of males and females isn't taken into account
    // So, it have to use countRabbit and countRabbit_MEM for calcul
    // All in all, calculate the number of rabbits suitable for childbirth
    // As long as there are male rabbits, there must be female rabbits !!!
    numAdultFemales = getAdultFemales(rabbit);
    stateMale = getAdultMale(rabbit);
    babyThisYear = 0;
    babies[0] = 0;
    babies[1] = 0;
            
    /* If there is at least one male */
    if (stateMale)
    {

        /* For all the females */
        for (int i = 0; i < numAdultFemales; i++)
        {
            timeThisYear = getTimesChildYear();

            /* For all the litters */
            for (int j = 0; j < timeThisYear; j++)
            {
                babyThisYear += getNbBaby();
            }
        }

        /* For all the babies born */
        for (int i = 0; i < babyThisYear; i++)
        {
            sex = judgeGender();
            babies[sex]++;
        }
    }

    // second, those who need to die >_<
     getDeaths(rabbit);

    // Add the babies (._.)
    rabbit->males[0] = babies[1];
    rabbit->females[0] = babies[0];

}


/** ------------------------------------------------------------------- *
 * @fn         realRabbit                                              *
 *                                                                     *
 * @brief      An algorithm for realistic simulation of rabbit         *
 *             populations                                             *
 *                                                                     *
 * @param      numStartBaby {int} Number of couples of baby rabbits    *
 * @param      numStartAdult {int} Number of couples of adult rabbits  *
 * @param      rabbit {Rabbit} Struct for the rabbits.                 *
 * @param      duration {int} Duration of the simulation               *
 * @param      meanF {unsigned long long int *} Array to count how many*
 *                    female rabbits there are for each year           *
 * @param      meanM {unsigned long long int *} Array to count how many*
 *                    male rabbits there are for each year             *
 * ------------------------------------------------------------------- */
void realRabbit(int numStartBaby, int numStartAdult, Rabbit *rabbit, int duration, unsigned long long int * meanF, unsigned long long int * meanM)
{
    long long int numMales;
    long long int numFemales;
    int           age;

    /* For each age, init the arrays */
    for (int i = 0; i < 16; i++)
    {
        rabbit->females[i] = 0;
        rabbit->males[i] = 0;
    }

    /* Initialize the baby rabbits */
    for (int i = 0; i < numStartBaby; i++)
    {
        rabbit->males[0]++;
        rabbit->females[0]++;
    }

    /* Initialize the adult rabbits with age from 1 to 4 */
    for (int i = 0; i < numStartAdult; i++)
    {
        age = 1 + 3 * genrand_real1();
        rabbit->males[age]++;
        rabbit->females[age]++;
    }

    /* Simulation for every year */
    for (int year = 1; year <= duration; year++)
    {
        oneYearLater(rabbit);

        // show off !_! the rabbits for the year
        numMales = 0;
        numFemales = 0;

        /* Count all the rabbits */
        for (int i = 0; i < 16; i++)
        {
            numMales += rabbit->males[i];
            numFemales += rabbit->females[i];
        }

        if(DEBUG) printf("After year %d, there are %lld males and %lld females\n", year, numMales, numFemales);
        meanF[year - 1] += numFemales;
        meanM[year - 1] += numMales;

    }

    if(DEBUG) printf("\n");
}

/** ------------------------------------------------------------------- *
 * @fn         multipleExperiments                                     *
 *                                                                     *
 * @brief      Simulates a certain number of rabbit population growth  *
 *                                                                     *
 * @param      numStartBaby {int} Number of couples of baby rabbits    *
 * @param      numStartAdult {int} Number of couples of adult rabbits  *
 * @param      rabbit {Rabbit} Struct for the rabbits.                 *
 * @param      duration {int} Duration of the simulation               *
 * @param      numExp {int} Number of simulations to be done           *
 * @return     mean for the population of rabbits                      *
 * ------------------------------------------------------------------- */
unsigned long long int multipleExperiments(int numStartBaby, int numStartAdult, Rabbit *rabbit, int duration, int numExp){
    unsigned long long int * meanF     = malloc(sizeof(unsigned long long int) * duration);
    unsigned long long int * meanM     = malloc(sizeof(unsigned long long int) * duration);
    unsigned long long int   meanTotal = 0;

    for(int d = 0; d < duration; d++){
        meanF[d] = 0;
        meanM[d] = 0;
    }

    /* For each experiment */
    for (int exp = 1; exp <= numExp; exp++)
    {
        
        if(DEBUG) printf("_________________________\n\tExperiment %d : \n", exp);

        realRabbit(numStartBaby, numStartAdult, rabbit, duration, meanF, meanM);
    

        if(DEBUG) printf("\n");
    }


    for(int d = 0; d < duration; d++){
        printf("_____________________\n\tMeans for year %d :\n\n", d + 1);
        printf("Females : %llu\n", meanF[d]/numExp);
        printf("Males : %llu\n", meanM[d]/numExp);
        meanTotal += meanM[d] + meanF[d];
    }

    free(meanF);
    free(meanM);

    return meanTotal/numExp;
}