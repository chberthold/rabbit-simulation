CC= gcc
CFLAGS= -Wall -Wextra -g -O2

rabbits: main.o lapins.o mt.o
		$(CC) $(CFLAGS) -o $@ $^ -lm

lapin.o: lapin.c mt.h
		$(CC) $(CFLAGS) -o $@ -c $< -lm

mt.o: mt.c
		$(CC) $(CFLAGS) -o $@ -c $< -lm

clean:
		rm *.o
		rm rabbits