#include <stdio.h>
#include "lapins.h"
#include "mt.h"

int main()
{
    /* Inisiation for the ramdon generator */
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    /*INIT*/

    static struct Rabbit Rabbit;


    //RabbitSwarmAlgo(DURATION);
    
    time_t start_time = 0;
    time_t end_time = 0;
    start_time = clock();
    printf("Start !\n");
    multipleExperiments(0, 1, &Rabbit, DURATION, 10);

    end_time = clock();
    printf("Time Used : %fs\n", (double)
        (end_time - start_time) / CLOCKS_PER_SEC);


    
    return EXIT_SUCCESS;
}