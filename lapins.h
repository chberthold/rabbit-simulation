#ifndef __LAPINS__
#define __LAPINS__

#include <stdio.h>
#include <math.h>
#include <stdlib.h> 
#include <string.h>
#include <time.h>
#include "mt.h"


#define DURATION 20
#define NUMRABBITSTART 2
#define DEBUG 0


typedef struct Rabbit
{
    long long int males[16]; // Arrays in which to store the numbers of rabbits, each cell representing the age of the rabbits
                             // [0] -> baby rabbits, [1] -> 1-year-old rabbits, ...
    long long int females[16];
}Rabbit;

int RabbitSwarmAlgo(int n);
int judgeGender(void);
int calculChanceSurvival(int ageRabbit);
double BoxMuller(void);
int getTimesChildYear(void);
int getNbBaby(void);
long long int getAdultFemales(Rabbit *rabbits);
int getAdultMale(Rabbit *rabbits);
void oneYearLater(Rabbit *rabbit);
void realRabbit(int numStartBaby, int numStartAdult, Rabbit *rabbit, int duration, unsigned long long int * meanF, unsigned long long int * meanM);
unsigned long long int multipleExperiments(int numStartBaby, int numStartAdult, Rabbit *rabbit, int duration, int numExp);

#endif